<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Repositories\Posts;
use App\Tag;

class PostsController extends Controller {

    public function __construct() {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index() {
   //     dd($posts);
        
        $posts = Post::latest();

        if (request(['month', 'year'])) {
            $posts->filter(request(['month', 'year']));
        }

        $posts = $posts->get();

        return view('posts.index', compact('posts', 'archives'));
    }

    public function show(Post $post) {
        // Traditional
        // $post = Post::find($id); // need $id arg in the show method declaration
        // Route Model Binding to Post arg
        return view('posts.show', compact('post'));
    }

    public function create() {
        return view('posts.create');
    }

    public function store() {
        //   dd(request('title'));
        $post = new Post;

        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required']);

//        $post->title = request('title');
//        $post->body = request('body');
//        $post->user_id = auth()->id();
//        $post->save();
        // OR

        auth()->user()->publish(new Post(request(['title', 'body'])));

        // OR
//        Post::create([
//            'title' => request('title'),
//            'body' => request('body'),
//            'user_id => auth()->id()
//        ]);
        // Redirect
        return redirect('/');
    }

}
