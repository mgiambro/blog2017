<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Mail\Welcome;
use App\Http\Requests\RegistrationForm;

class RegistrationController extends Controller {

    public function create() {
        return view('registrations.create');
    }

    public function store(RegistrationForm $form) {
        // Validate the form (done by the RegistrationRequest) 
        // Use in controller for simple validation. Few fields etc..

//        $this->validate(request(), [
//            'name' => 'required',
//            'email' => 'required|email',
//            'password' => 'required|confirmed'
//        ]);

        // Create and save the user
//        $user = User::create([
//        'name' => request('name'),
//        'email' => request('email'),
//        'password' => bcrypt(request('password'))
//        ]);

        // Sign them in
        // \Auth::login();
//        auth()->login($user);
//        
//        \Mail::to($user)->send(new Welcome($user));

        // Redirect to the home page
        //  return redirect('/');
        
                $form->persist();
               
        session()->flash('message', 'Thanks so much for signing up!');      
        
        return redirect()->home();
    }

}
