<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;

class CommentsController extends Controller
{
    public function store(Post $post)
    {
       // Longform
//       Comment::create([
//           'body' => request('body'),
//           'post_id' => $post->id
//       ]);
        
        $this->validate(request(), ['body' => 'required|min:2']);
        // Better represents the action
        $post->addComment(request('body'));
       
       return back();
    }
}
