<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
/**
 * Description of Model
 *
 * @author Maurizio
 */
class Model extends Eloquent
{

    protected $guarded = ['user_id']; // Blacklist

}
