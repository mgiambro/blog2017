// To edit the css of these components, php artisan vendor:publish --tag=laravel-mail.
// This copies the files from the vendor laravel directories to /resources/views/vendor/mail where you can edit.
// Can do this with other files than the laravel mail files.

@component('mail::message')
# Introduction

The body of your message.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

@component('mail::panel', ['url' => ''])
Lorem ipsum dolara sit amet.
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
